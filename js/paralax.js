    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type"/>
    
    <title>Scrolling Parallax Plugin for jQuery - Sorry direct downloading is not allowed</title>
    
    <meta name="Description" content="" />
    <meta name="ROBOTS" content="NOINDEX" />
    <link rel="stylesheet" type="text/css" href="/css/docs.css" />
    
    
    </head>
    
    <body>
    
    <div id="header">
        <a href="/">dev.jonraasch.com</a>
    </div>
    
    <div id="sidebar">
            <script type="text/javascript"><!--
        google_ad_client = "pub-9859037563312476";
        /* 160x600 dev.jonraasch.com */
        google_ad_slot = "7941909703";
        google_ad_width = 160;
        google_ad_height = 600;
        //-->
        </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>
    </div>
    
    <div id="content">            <h1>
            Sorry direct access to downloads is not allowed
            </h1>
            
            <p>
            But you can download what you were looking for using the link below:
            </p>
            
            <a href="download" class="gray-button" rel="nofollow">Download Scrolling Parallax Plugin for jQuery &rarr;<span class="gray-button-cap"></span></a>
            
            <br class="clear" />
            
                        <p>
            Or check out the docs:
            </p>
            
            <a href="docs" class="gray-button">Scrolling Parallax Plugin for jQuery Documentation &rarr;<span class="gray-button-cap"></span></a>
            
            <br class="clear" />
            
                    <br /><br />
    
         
        
        <p>
        <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=4URDTZYUNPV3J&lc=US&item_name=Jon%20Raasch&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHosted" class="gray-button" rel="nofollow">Make A Donation &rarr;<span class="gray-button-cap"></span></a>
        </p>
        
    </div>
    
    <p id="footer">
    Copyright &copy;2009 <a href="http://jonraasch.com/">Jon Raasch</a> - All rights reserved
    </p>
    
    <script type="text/javascript">
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
    try {
    var pageTracker = _gat._getTracker("UA-3123668-20");
    pageTracker._trackPageview();
    } catch(err) {}</script>
    
    </body>
    </html>